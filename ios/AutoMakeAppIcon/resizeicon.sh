#程强
#! /bin/bash


# prepare
ROOT_DIR=$(pwd)

#check file exist
#设置1024尺寸的原图片路径
SOURCE_FILE="${ROOT_DIR}/Desktop/AutoMakeAppIcon/1024.png"
echo $SOURCE_FILE
if [[ ! -e ${SOURCE_FILE} ]]; then
	echo "文件不存在"
	exit 1
fi
#设置自动切割生成制定尺寸图片的位置
DEST_DIR="${ROOT_DIR}/Desktop/AutoMakeAppIcon/AppIcon"
#如果目录有图片先清空
if [[ -d ${DEST_DIR} ]]; then
	rm -rf dir ${DEST_DIR}
fi
mkdir -p "${DEST_DIR}"
#图片自定义的名称
Image_NAME=("Icon-Samll-20.png" "Icon-Small-20@2x.png" "Icon-Small-29.png" "Icon-Small-29-Small@2x.png" "Icon-Small-29@3x.png" "Icon-Small-40@.png" "Icon-Small-40@2x.png" "Icon-29.png" "Icon-29@2x.png" "Icon-29@3x.png" "Icon-40.png" "Icon-40@2x.png" "Icon-40@3x.png" "Icon-87.png" "Icon-57.png" "Icon-57@2x.png" "Icon-76.png" "Icon-76@2x.png" "Icon-60@2x.png" "Icon-60@3x.png" "Icon-72.png" "Icon-72@2x.png" "Icon-83.5@2x.png")
#图片对应的尺寸
Image_SIZE=("20" "40" "29" "58" "87" "40" "80" "29" "58" "87" "40" "80" "120" "87" "57" "114" "76" "152" "120" "180" "72" "144" "167")


#sips starting
cp "${SOURCE_FILE}" "${DEST_DIR}"
for ((i=0; i<${#Image_SIZE[@]} ;i++)); do
	size=${Image_SIZE[i]}
	sips -Z ${size} "${SOURCE_FILE}" --out "${DEST_DIR}/${Image_NAME[i]}"
done


